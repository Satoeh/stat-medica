\section{Analisi Esplorativa}
Nell'analisi esplorativa viene svolta una preliminare descrizione delle informazioni date dalle variabili osservate. Si individuano inoltre le relazioni tra coppie di tali variabili mendiante opportuni test statistici, dove, si assumer? una soglia di significativit? pari a $0.05$.

\subsection{Analisi univariata}
Per le variabili categoriali, possono essere valutate le numerosit? nei gruppi.

<<univ_gruppi_curve>>=
latexTable(tabfreq(CURVE, digits=2, totali=FALSE), caption = "Numerosit? campionaria e relativa proporzione per ogni esperimento eseguito.", align ="cc")
@

<<univ_herbicide, fig.cap="Grafico a torta della variabile \\textit{herbicide}.", message = F, warning = F>>=
table1 <- table(HERBICIDE)
cols<-c("lightyellow","orange")
labs<-c("Bentazone", "Diuron")
# etichette con percentuali
pct<-round((table1/margin.table(table1)*100),1)
lbls<-paste(pct,"%",sep="")
pie(table1,main="Erbicida", labels = lbls, col=cols)
#legenda
legend(0.7, 1.0, cex = 0.6, legend=labs, fill = cols)

# 0.7, 1.0 = coordinate della legenda;
# cex = dimensioni della legenda
@

Si nota dalla tabella \ref{tab:univ_gruppi_curve} che per ogni esperimento sono state utilizzate $21$ piante di spinaci, dunque vi ? la stessa proporzione ($20\%$) di unit? statistiche per ciascuno dei cinque esperimenti effettuati.
Per quanto riguarda la variabile \textit{herbicide} si osserva un maggior utilizzo dell'erbicida \textit{bentazone} rispetto a \textit{diuron}; in particolar modo si osserva che il rapporto $bentazone/diuron$ ? $\Sexpr{table1[1]/table1[2]}$.

\paragraph{}\mbox{}\\
Nel caso della variabile \textit{dose} sono stati fissati a priori determinati livelli di concentrazione dei due erbicidi e successivamente si ? osservato come le unit? statistiche hanno reagito a tale somministrazione.

<<univ_dose_n>>=
latexTable(t(as.matrix(summary(DOSE))), digits=3, caption = "Principali indici descrittivi per la variabile \\textit{dose}.", align ="cc")
@

\paragraph{}\mbox{}\\
La variabile \textit{Slope} ? quantitativa continua; se ne possono dunque osservare le principali statistiche descrittive e vedere come tale variabile si distribuisce attraverso un'analisi grafica.

<<univ_slope, fig.cap="Istogramma e boxplot della variabile \\textit{slope}.", message = F, warning = F>>=

# Sommario della variabile slope, con standard deviation
summary(SLOPE) %>% as.matrix() %>% rbind(sd(SLOPE)) %>% t -> tab
colnames(tab)[length(tab)] = "Std. dev."
latexTable(tab, caption = "Statistiche descrittive per la variabile \\textit{slope}.", align = "cc", digits = 3)

# Grafico della variabile SLOPE
grid.arrange(
  ggplot(spinach, aes(x = SLOPE, y = ..density..)) +
    geom_histogram(bins = 10, col = "black", fill = "darkgray") +
    geom_density(col = "darkblue", fill = "blue", alpha = 0.13) +
    scale_x_continuous(limits = c(min(SLOPE), max(SLOPE)))+
    theme(aspect.ratio = 1),
  ggplot(spinach, aes(x = "", y=SLOPE)) +
    stat_boxplot(geom = "errorbar")+
    geom_boxplot()+
    xlab("")+
    theme(aspect.ratio = 1),
  ncol = 2)
# indice di asimmetria di pearson
swek <- function(x){
  n <- length(x)
  s3 <- sqrt(var(x)*(n-1)/n)^3
  mx <- mean(x)
  sk <- sum((x-mx)^3)/s3
  sk/n
}
@

Si osserva che i valori pi? probabili per la variabile \textit{slope} si registrano in corrispondenza di basse quantit? d'ossigeno consumate.
La classe modale ? quella che fa riferimento ai valori pi? bassi per la variabile e si nota che la mediana ? minore della media; la distribuzione infatti, come si pi? notare dalla figura \ref{fig:univ_slope}, presenta una asimmetria sulla coda destra: l'\textit{indice di asimmetria di Pearson} \cite{ventura2017} risulta infatti essere \Sexpr{round(swek(SLOPE),3)}.

Si passa allora a verificare l'ipotesi di normalit? per tale variabile.
Si utilizza il test di normalit? di \textit{Shapiro Wilk}\cite{grigoletto2017} il quale fornisce un $p-value$ $\doteq$ \Sexpr{round(shapiro.test(SLOPE)$p, 3)} e porta dunque al rifiuto dell'ipotesi nulla.
<<univ_shapiro_slope>>=
sh <- shapiro.test(SLOPE)
cbind("W" = as.numeric(sh$statistic), "p-value" = sh$p.value) %>%
  round(3) %>%
  latexTable(caption = "Valore osservato del test di Shapiro Wilk e relativo p-value.", align ="cc")
@
<<qqplot_slope, fig.cap="Diagramma quantile-quantile normale per la variabile \\textit{slope}.">>=
ggplot_qqplot(SLOPE)+theme(aspect.ratio = 1)
@
La figura \ref{fig:qqplot_slope} evidenzia un allontanamento dall'ipotesi di normalit? per code pesanti e ci? ? in accordo con l'asimmetria emersa in precedenza.

\subsection{Analisi bivariata}
In primo luogo osservo come ? stata impostata l'analisi con le variabili qualitative \textit{curve} ed \textit{herbicide}.
In particolar modo si osserva che i primi tre esperimenti sono stati fatti utilizzando \textit{bentazone} come erbicida, mentre per gli altri due si ? utilizzato \textit{diuron}.
<<curve_herbicide, eval = T>>=
table(CURVE, HERBICIDE) %>% as.matrix %>% 
  latexTable(row.names = T, caption ="Numerosit? di piante per le varie combinazioni di \\textit{curve} e \\textit{herbicide}.", align = "c") %>%
  add_header_above(c(" " = 1, "Herbicide" = 2), italic = T, line = F) %>%
  group_rows(index = c("Curve" = 5), bold = F, italic = T, colnum = 1)
@

% I DATI PER CURVE E BENTAZIONE SONO STATI FISSATI A PRIORI QUINDI NON DEVO
% FARE NESSUNA VERIFICA DI DIPENDENZA

Analogamente, dalle figura \ref{fig:plot_curve_dose_herbicide}, si osserva come si fa largo uso di dosi piuttosto basse per entrambi gli erbicidi, mentre solo per i primi tre esperimenti, ovvero quelli che fanno riferimento all'erbicida \textit{bentazone}, si utilizzano dosi molto elevate.
<<plot_curve_dose_herbicide, fig.cap="Rappresentazione grafica della variabile \\textit{dose} per i due erbicidi nei cinque esperimenti.">>=
ggplot(spinach, aes(factor(CURVE), DOSE))+
  geom_boxplot() + facet_grid(~HERBICIDE) + ylab("dose")+xlab("curve")+theme(aspect.ratio = 1)
@

% ANCHE QUA!!
% I DATI PER CURVE E BENTAZIONE E HERBICIDE SONO STATI FISSATI A PRIORI QUINDI NON DEVO FARE NESSUNA VERIFICA DI DIPENDENZA
\paragraph{}\mbox{}\\
Si passa dunque a studiare come la variabile \textit{slope} sia influenzata dalla variabile \textit{herbicide} e dalle relative dosi.

I due gruppi per la variabile \textit{slope} condizionati ai due erbicidi \textit{bentazone} e \textit{diuron} hanno ripettivamente $\Sexpr{length(SLOPE[HERBICIDE=="bentazon"])}$ e $\Sexpr{length(SLOPE[HERBICIDE=="diuron"])}$ osservazioni l'uno.
<<box_herbicide_slope, fig.cap="Boxplot della variabile \\textit{slope} rispetto all'erbicida utilizzato.">>=
  ggplot(spinach, aes(x = HERBICIDE, y = SLOPE))+
    stat_boxplot(geom = "errorbar") +
    geom_boxplot()+
    ylab("slope")+xlab("herbicide")+theme(aspect.ratio = 1)
@

<<ist_herbicide_slope, fig.cap="Istogrammi della variabile \\textit{slope} rispetto all'erbicida utilizzato.">>=

  ggplot(spinach, aes(x = SLOPE, y = ..density.., color = HERBICIDE)) +
    xlab("slope")+
    geom_histogram(bins = 10, col = "black", fill = "darkgray") +
    geom_density(aes(fill = HERBICIDE), alpha = 0.13, show.legend = F) +
    scale_color_manual(values = c("darkred", "darkblue")) +
    scale_fill_manual(values = c("red", "blue")) +
    # scale_x_continuous(limits = c(min(SLOPE), max(SLOPE)))+
    # theme(aspect.ratio = 1) +
    facet_wrap(~HERBICIDE)+theme(aspect.ratio = 1)
@

Dalla figure \ref{fig:box_herbicide_slope} e \ref{fig:ist_herbicide_slope}, si nota una differenza significativa delle due distribuzioni condizionatamente all'erbicida utilizzato. La distribuzione relativa all'erbicida \textit{bentazone} presenta valori di media e mediana molto pi? bassi, una spiccata asimmetria positiva (indice di asimmetria di Pearson $\doteq$ \Sexpr{(round(swek(SLOPE[HERBICIDE=="bentazon"]), 3))}), ed un range di variabilit? molto pi? contenuto, rispetto alla distribuzione del consumo d'ossigeno registrata dopo la somministrazione dell'inibitore sintetico \textit{diuron} (dove si osserva un indice di asimmetria di Pearson $\doteq$ \Sexpr{(round(swek(SLOPE[HERBICIDE=="diuron"]), 3))}, molto pi? prossimo allo zero).

Si passa allora a valutare se i due erbicidi hanno in media lo stesso effetto sul consumo d'ossigeno.
<<qqplot_slope_erbicida, fig.cap="Diagramma quantile-quantile normale per la variabile \\textit{slope} condizionatamente all'erbicida bentazone, a destra, e diuron, a sinistra.">>=
grid.arrange(
  ggplot_qqplot(SLOPE[HERBICIDE=="bentazon"])+theme(aspect.ratio = 1),
  ggplot_qqplot(SLOPE[HERBICIDE=="diuron"])+theme(aspect.ratio = 1),
  ncol=2)
@
Si studia la normalit? dei due gruppi: come ci si pu? attendere dalle precedenti considerazioni sulla simmetria delle due distribuzioni, l'applicazione del test di normalit? di Shapiro Wilk porta al rifiuto dell'ipotesi di normalit? per entrambi i gruppi, con $W_{oss}$ = $\Sexpr{round(shapiro.test(SLOPE[HERBICIDE=="bentazon"])$stat, 3)}$ per il gruppo con \textit{bentazone}, e $W_{oss}$ = $\Sexpr{round(shapiro.test(SLOPE[HERBICIDE=="diuron"])$stat, 3)}$ nell'altro gruppo, e con $p-value$ $<$ $0.01$ in entrambi i casi.
\\[0.5cm]
Applicando il test di Levene \cite{ventura2017} per l'ipotesi di omogeneit? delle varianze nei due gruppi si ha un valore della statistica osservata pari a $T_{oss}$ = \Sexpr{round(leveneTest(SLOPE, HERBICIDE)$F[1], 3)} con un conseguente $p-value$ $\doteq$ \Sexpr{round(leveneTest(SLOPE, HERBICIDE)$Pr[1], 3)} che porta dunque a rifiutare tale ipotesi.
\\[0.5cm]
Date tali conclusioni, che portano a dire che le due distribuzioni hanno forme e variabilit? molto diverse tra loro, si passa ad applicare il test di Kolmogorov-Smirnov \cite{ventura2017} per verificare l'ipotesi che entrambi i campioni provengano da una stessa popolazione.
<<ecdf_slope_erbicida, fig.cap="Grafico delle funzioni di ripartizione di \\textit{slope} condizionatamente ai due erbicidi utilizzati.">>=
ggplot(spinach, aes(SLOPE, col=HERBICIDE)) + stat_ecdf(geom = "step") +theme(aspect.ratio = 1)
@
Come evidenzia anche la figura \ref{fig:ecdf_slope_erbicida}, il test porta al rifiuto dell'ipotesi nulla ($p-value$ < $0.01$ e $KS_{oss}$ $\doteq$ \Sexpr{round(0.50794, 3)}). Dunque vi ? una differenza significativa nella distribuzione della variabile \textit{slope} nei due gruppi di piante.

\paragraph{}\mbox{}\\
Passando infine a valutare come la variabile \textit{dose} influenza la variabile \textit{slope}, si osserva che a dosi pi? basse corrisponde un consumo pi? elevato della quantit? d'ossigeno da parte delle piante trattate con entrambi gli inibitori sintetici. Aumentando la dose, come ci si aspetta, diminuisce la quantit? d'ossigeno consumato durante la fotosintesi: le due variabili presentano una correlazione negativa \cite{ventura2017} ($\doteq$ \Sexpr{round(cor(DOSE, SLOPE), 3)}), dunque i due erbicidi hanno un effetto significativo sulla variabile \textit{slope}.

<<suppress_mess, message = F, warning = F, results='hide'>>=
spi <- as.data.frame(cbind(CURVE[HERBICIDE=="bentazon"], HERBICIDE[HERBICIDE=="bentazon"], DOSE[HERBICIDE=="bentazon"], SLOPE[HERBICIDE=="bentazon"]))
spi2 <- as.data.frame(cbind(CURVE[HERBICIDE=="diuron"], HERBICIDE[HERBICIDE=="diuron"], DOSE[HERBICIDE=="diuron"], SLOPE[HERBICIDE=="diuron"]))
colnames(spi) <- c("v1", "v2", "doseB", "slopeB")
colnames(spi2) <- c("v1", "v2", "doseD", "slopeD")
attach(spi)
attach(spi2)
@
Le stesse considerazioni sono valide se si considerano separatamente i due erbicidi sintetici; come si nota anche dai due grafici di dispersione, con \textit{bentazone} si ha una correlazione $cor_{benatzone}$ $\doteq$ \Sexpr{round(cor(spi[,3], spi[,4]), 3)}, e con \textit{diuron} si ha $cor_{diuron}$ $\doteq$ \Sexpr{round(cor(spi2[,3], spi2[,4]), 3)}.
<<biv_slope_dose, fig.cap="Diagramma di dispersione per le variabili \\textit{slope} e \\textit{dose} per i due erbicidi.">>=
grid.arrange(
 ggplot(spi, aes(x = spi[,3], y = spi[,4])) +
    geom_point()+xlab("dose di bentazone")+ylab("slope")+theme(aspect.ratio = 1),
 ggplot(spi2, aes(x = spi2[,3], y = spi2[,4])) +
    geom_point()+xlab("dose di diuron")+ylab("slope")+theme(aspect.ratio = 1),
 ncol=2)
@

