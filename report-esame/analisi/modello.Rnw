\section{Modello Dose Risposta}
Si studia ora un modello di regressione\cite{ventura2017} per lo studio della relazione tra la variabile esplicativa \code{co2c} e la risposta \code{co2ur}. Il modello proposto è il seguente\cite{Potvin1990}:

\begin{equation}\label{eq:model}
y = A\left[1 - e^{-k(x-x_0)}\right] \,.
\end{equation}

I coefficienti del modello hanno la seguente interpretazione: $A$ rappresenta il valore limite a cui tende y, all'aumentare di x; $k > 0$ è l'intensità della dipendenza da x; $x0$ è un parametro di posizione.\\
Dalla Figura~\ref{fig:co2_co2ur_id}, gli andamenti al variare di \code{co2c} appaiono omogenei di forma in ciascuno dei gruppi. Il modello verrà stimato per ciascuno di essi e successivamente confrontato.

<<co2_co2ur_id, fig.cap="Diagramma di dispersione della variabile \\code{co2ur} rispetto a \\code{co2c}.", out.height="0.75\\textwidth", out.width = "0.75\\textwidth", fig.height=5, fig.width=5>>=
ecophys %>% 
  transform(treatment = c("treatment: 1", "treatment: 2")[as.numeric(treatment)],
            location = c("location: 1", "location: 2")[as.numeric(location)]) %>%
  ggplot(aes(x = co2c, y = co2ur, group = interaction(id, treatment, location))) +
    geom_point() +
    geom_line() +
    facet_grid(treatment ~ location)
@

La procedura di stima del modello viene eseguita tramite minimi quadrati non lineari ({\em Nonlinear Least Squares}). Come stima preliminare dei parametri, è stata individuata per via grafica (Figura~\ref{fig:graphicmodel}) una combinazione di valori adeguati: $A = 47, k = 0.0025, x_0 = 4$.

<<graphicmodel, fig.cap="Diagramma di dispersione della variabile \\code{co2ur} rispetto a \\code{co2c} con curva iniziale in blu.", out.height="0.7\\textwidth", out.width = "0.7\\textwidth", fig.height=4.5, fig.width=4.5>>=
ggplot(aes(x = co2c, y = co2ur), data = ecophys) +
  geom_point() +
  stat_function(aes(x = co2c), fun = function(x){47*(1 - exp(-0.0025*(x-4)))},
                color = "darkblue", size = 0.73)
@

<<modello, warning = F, message = F>>=
#---- Bisogna fare il modello per ciascuno dei gruppi. Questo è di tutti i gruppi, solo per vedere se effettivamente funziona nls.
parstart = list(A = 47, k = 0.0025, x0 = 4)
# Mississippi e controlli
ymc <- co2ur[treatment=="1"&location=="2"]
xmc <- co2c[treatment=="1"&location=="2"]
fitMC <-  nls(ymc~ A*(1 - exp(-k * (xmc-x0))), start = parstart)

sumfitMC = summary(fitMC)

# Mississipi e freddi
ymf <- co2ur[treatment=="2"&location=="2"]
xmf <- co2c[treatment=="2"&location=="2"]
fitMF <-  nls(ymf~ A*(1 - exp(-k * (xmf-x0))), start = parstart)

parMF <- summary(fitMF)$coef[,1]
sumfitMF = summary(fitMF)

# Quebec e freddi 
yqf <- co2ur[treatment=="2"&location=="1"]
xqf <- co2c[treatment=="2"&location=="1"]
fitQF <-  nls(yqf~ A*(1 - exp(-k * (xqf-x0))), start = parstart)
parQF <- summary(fitQF)$coef[,1]

sumfitQF = summary(fitQF)

# Quebec e controllo 
yqc <- co2ur[treatment=="1"&location=="1"]
xqc <- co2c[treatment=="1"&location=="1"]
fitQC <-  nls(yqc~ A*(1 - exp(-k * (xqc-x0))), start = parstart)
parQC <- summary(fitQC)$coef[,1]

sumfitQC = summary(fitQC)

summaryConvert = function(fitsum, se = T){
  if(se){
   fitsum$coefficients %>%
    rbind(., c(fitsum$sigma^2, NA, NA, NA)) %>%
    as.data.frame %>%
    cbind(" " = c(" ", " ", " ", " "), "Coef." = c("$A$", "$k$", "$x_0$", "$S^2$"), .) %>%
    set_names(c(" ", "Coef.", "Estimate", "Std. Error", "$t_{oss}$", "\\textit{p-value}"))   
  }
  else{
  fitsum$coefficients %>%
    as.data.frame %>%
    cbind(" " = c(" ", " ", " "), "Coef." = c("$A$", "$k$", "$x_0$"), .) %>%
    set_names(c(" ", "Coef.", "Estimate", "Std. Error", "$t_{oss}$", "\\textit{p-value}"))
  }
   
}

rbind(sumfitQC %>% summaryConvert, sumfitQF %>% summaryConvert,
      sumfitMC %>% summaryConvert, sumfitMF %>% summaryConvert) %>%
  latexTable(align = "c", digits = c(1,1,4,4,2,4), row.names = F, escape = F,
             caption = "Coefficienti stimati per i modelli.") %>%
  group_rows(index = c("\\\\code{location}: 1 \\\\hspace{3pt} \\\\code{treatment}: 1" = 4, 
                       "\\\\code{location}: 1 \\\\hspace{3pt} \\\\code{treatment}: 2" = 4,
                       "\\\\code{location}: 2 \\\\hspace{3pt} \\\\code{treatment}: 1" = 4,
                       "\\\\code{location}: 2 \\\\hspace{3pt} \\\\code{treatment}: 2" = 4),
             colnum = 4, escape = F, bold = F, extra_latex_after = "\\addlinespace[4pt]")
@

Assieme alle stime dei coefficienti, nella Tabella~\ref{tab:modello} vengono riportate le stime non distorte della varianza residua, $S^2$, per ciascun modello. I modelli stimati risultano essere, quindi:

\begin{align*}
\hat{y} = \Sexpr{round(coef(fitQC), 2)["A"]}\left[1 - e^{\Sexpr{-round(coef(fitQC), 3)["k"]} (x - \Sexpr{round(coef(fitQC), 2)["x0"]})}\right] \,, \\
\hat{y} = \Sexpr{round(coef(fitQF), 2)["A"]}\left[1 - e^{\Sexpr{-round(coef(fitQF), 3)["k"]} (x - \Sexpr{round(coef(fitQF), 2)["x0"]})}\right] \,, \\
\hat{y} = \Sexpr{round(coef(fitMC), 2)["A"]}\left[1 - e^{\Sexpr{-round(coef(fitMC), 3)["k"]} (x - \Sexpr{round(coef(fitMC), 2)["x0"]})}\right] \,, \\
\hat{y} = \Sexpr{round(coef(fitMF), 2)["A"]}\left[1 - e^{\Sexpr{-round(coef(fitMF), 3)["k"]} (x - \Sexpr{round(coef(fitMF), 2)["x0"]})}\right] \,.
\end{align*}

<<co2_co2ur_id_stima, fig.cap="Diagramma di dispersione della variabile \\code{co2ur} rispetto a \\code{co2c} nei gruppi, con il modello stimato in blu.", warning = F, message = F, out.height="0.68\\textwidth", out.width = "0.68\\textwidth", fig.height = 4.5, fig.width = 4.5>>=
  
cbind(ecophys, "pred" = c(fitQC %>% predict, fitQF %>% predict,
                                fitMC %>% predict, fitMF %>% predict)) %>%
  transform(treatment = c("treatment: 1", "treatment: 2")[as.numeric(treatment)],
            location = c("location: 1", "location: 2")[as.numeric(location)]) %>%
  ggplot(aes(x = co2c, y = co2ur, group = interaction(id, treatment, location))) +
    geom_point() +
    geom_line() +
    geom_line(aes(x = co2c, y = pred), color = "blue", size = 0.73) +
    facet_grid(treatment ~ location) 
@

<<qqplot_residui, fig.cap="Diagramma quantile-quantile normale per i residui dei modelli.", out.height="0.68\\textwidth", out.width = "0.68\\textwidth", fig.height = 4.5, fig.width = 4.5>>=
c(as.numeric(residuals(fitQC)), as.numeric(residuals(fitQF)),
  as.numeric(residuals(fitMC)), as.numeric(residuals(fitMF))) %>%
  ggplot_qqplot(data = ecophys %>% 
                  transform(treatment = c("treatment: 1", "treatment: 2")[as.numeric(treatment)], location = c("location: 1", "location: 2")[as.numeric(location)])
                ) +
  facet_grid(treatment ~ location)
@

<<Woss_residui>>=
tab2 = matrix(NA, nrow = 4, ncol = 2)

tab2[1,] = c(shapiro.test(as.numeric(residuals(fitQC)))$p.value,
              shapiro.test(as.numeric(residuals(fitQC)))$statistic)
tab2[2,] = c(shapiro.test(as.numeric(residuals(fitMC)))$p.value,
              shapiro.test(as.numeric(residuals(fitMC)))$statistic)
tab2[3,] = c(shapiro.test(as.numeric(residuals(fitQF)))$p.value,
              shapiro.test(as.numeric(residuals(fitQF)))$statistic)
tab2[4,] = c(shapiro.test(as.numeric(residuals(fitMF)))$p.value,
              shapiro.test(as.numeric(residuals(fitMF)))$statistic)

colnames(tab2) = c("$W^{oss}$", "{\\em p-value}")
rownames(tab2) = c("\\code{treatment}: 1, \\code{location}: 1",
                   "\\code{treatment}: 1, \\code{location}: 2",
                   "\\code{treatment}: 2, \\code{location}: 1",
                   "\\code{treatment}: 2, \\code{location}: 2")
latexTable(tab2, escape = F, digits = 2, caption = "Statistica osservata e p-value del test di Shapiro-Wilk per i residui dei quattro modelli stimati.")
@

<<attesi_stimati_residui, fig.cap="Grafico di dispersione dei residui contro i valori stimati per i quattro modelli.", out.height="0.75\\textwidth", out.width = "0.75\\textwidth">>=
cbind(ecophys,
      "pred" = c(fitQC %>% predict, fitQF %>% predict,
                 fitMC %>% predict, fitMF %>% predict),
      "res" = c(fitQC %>% residuals, fitQF %>% residuals,
                fitMC %>% residuals, fitMF %>% residuals)) %>%
  transform(treatment = c("treatment: 1", "treatment: 2")[as.numeric(treatment)],
            location = c("location: 1", "location: 2")[as.numeric(location)]) %>%
  ggplot(aes(x = pred, y = res, group = interaction(id, treatment, location))) +
    geom_point() +
    facet_grid(treatment ~ location) +
    ylab("Residuals") +
    xlab("Fitted values")
@

Come evidenziato dai grafici dei residui, i modelli di regressione rappresentano bene la relazione tra la concentrazione e l'assorbimento di $\mathrm{CO}_2$ per le piante nelle quattro situazioni. Dalla Tabella~\ref{tab:Woss_residui} e dalla Figura~\ref{fig:qqplot_residui} si osserva che la normalità per i residui viene accettata in tutti i modelli ad un livello $\alpha = 0.05$. I residui, tuttavia, appaiono graficamente eteroschedastici (Figura~\ref{fig:attesi_stimati_residui}), con una maggiore varianza all'aumentare del valore della concentrazione stimata.\\
Confrontando i modelli stimati, si può osservare che le piante reagiscono differentemente al trattamento, in base alla provenienza: nel caso delle piante provenienti dal Québec, il coefficiente $A$ si riduce di appena il $\Sexpr{100*(1 - round(coef(fitQF)["A"]/coef(fitQC)["A"], 2))}\%$. Per le piante provenienti dal Mississippi, invece, la riduzione è del $\Sexpr{100*(1 - round(coef(fitMF)["A"]/coef(fitMC)["A"], 2))}\%$.

