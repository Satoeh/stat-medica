\section{Analisi della varianza a due vie}
Terminata l'analisi esplorativa, viene valutato l'effetto congiunto delle variabili \textit{stato} e \textit{disturbo} sulle variabili \textit{manitolo} e \textit{lattosio}.
\subsection{ANOVA a due vie per \textit{manitolo}}
Per valutare la presenza di effetti di interazione, è possibile utilizzare il grafico di interazione \cite{grigoletto2017} in figura \ref{fig:int_manitolo}.

<<int_manitolo, fig.cap = "Interaction plot per la variabile \\textit{manitolo}.">>=
ggplot(hiv, aes(x = stato, y = manitolo, colour = disturbo, group = disturbo)) +
  stat_summary(fun.y = mean, geom = "point") +
  stat_summary(fun.y = mean, geom = "line") +
  scale_color_manual(values = c("red", "blue")) +
  ylab("mean of manitolo") +
  theme(aspect.ratio = 1)
@

Si può osservare interazione fra le variabili: la media di \textit{manitolo} aumenta nel passare dal gruppo con stato pari a $1$ al gruppo con stato pari a $2$, quando disturbo vale $0$. Al contrario diminuisce, quando disturbo vale $1$. Nonostante queste osservazioni, non è possibile modellare il termine di interazione, a causa della scarsa numerosità campionaria nei gruppi.
<<anova2v_manitolo>>=
  lm(manitolo~disturbo + stato) %>%
  anova %>%
  as.matrix %>%
  .[,-2] %>%
  latexTable(digits = 2, row.names = T, caption = "ANOVA a due vie per la variabile \\textit{manitolo}.", col.names = c("Df.", "Mean Sq.", "F value", "\\textit{p-value}"), escape = F)
@

Dalla tabella \ref{tab:anova2v_manitolo}, l'effetto del gruppo \textit{disturbo} è significativo al $10\%$, considerando anche la presenza di \textit{stato}. Un'analisi tramite modello lineare \cite{grigoletto2017}, tuttavia, non rifiuta l'ipotesi di omogeneità delle medie ad un \textit{p-value} pari a \Sexpr{
x = summary(lm(manitolo~disturbo+stato))$fstatistic
round(pf(x[1],x[2],x[3], lower.tail = F),2)
}\,. Si può, quindi, concludere che \textit{stato} e \textit{disturbo} non influenzano significativamente l'assorbimento di mannitolo.

\subsection{ANOVA a due vie per \textit{lattosio}}
Con le stesse considerazioni, viene valutato l'effetto congiunto di \textit{stato} e \textit{disturbo} su \textit{lattosio}.

<<int_lattosio, fig.cap = "Interaction plot per la variabile \\textit{lattosio}.">>=
ggplot(hiv, aes(x = stato, y = lattosio, colour = disturbo, group = disturbo)) +
  stat_summary(fun.y = mean, geom = "point") +
  stat_summary(fun.y = mean, geom = "line") +
  scale_color_manual(values = c("red", "blue")) +
  ylab("mean of lattosio") +
  theme(aspect.ratio = 1)
@

Dal grafico, l'interazione appare meno evidente, in quanto le linee blu e rossa tendono entrambe a decrescere. Attraverso l'analisi della varianza, senza considerare termine di interazione, si ottiene:

<<anova2v_lattosio>>=
  lm(lattosio~disturbo + stato) %>%
  anova %>%
  as.matrix %>%
  .[,-2] %>%
  latexTable(digits = 2, row.names = T, caption = "ANOVA a due vie per la variabile \\textit{lattosio.}", col.names = c("Df.", "Mean Sq.", "F value", "\\textit{p-value}"), escape = F)
@

In questo caso, \textit{disturbo} è significativo al $5\%$ e, attraverso l'analisi del modello lineare, l'omogeneità delle medie viene rifiutata ad un \textit{p-value} di \Sexpr{
x = summary(lm(manitolo~disturbo+stato))$fstatistic
round(pf(x[1],x[2],x[3], lower.tail = F),2)
}\,. Con tale modello lineare, si possono analizzare i residui:

<<resid_qq, fig.cap = "Diagramma di dispersione e grafico quantile-quantile dei residui del modello lineare per la variabile \\textit{lattosio}", fig.width="0.97\\linewidth">>=
fit = lm(lattosio ~ disturbo + stato)
res = residuals(fit)
yHat = fitted(fit)
gridExtra::grid.arrange(
  ggplot(data.frame("fitted" = yHat,"residuals" = res), aes(x = fitted, y = residuals)) +
    geom_point() +
    theme(aspect.ratio = 1),
  ggplot_qqplot(res) +
    theme(aspect.ratio = 1),
  ncol = 2)
@

Il test di normalità di Shapiro-Wilk, applicato ai residui, rifiuta l'ipotesi di normalità ad un \textit{p-value} di \Sexpr{round(shapiro.test(residuals(fit))$p.value, 4)}\,. Dal grafico, si può notare anche la presenza di eteroschedasticità nei residui. L'ipotesi di omoschedasticità dei residui viene rifiutata dal test di Breusch-Pagan \cite{Breusch1979}, con \textit{p-value} pari a \Sexpr{round(bptest(fit)$p.value, 3)}. Con tali considerazioni, la significatività dell'effetto della variabile \textit{disturbo} non può essere ritenuta valida.

\section{Conclusioni}
L'analisi, nel suo complesso, conduce alle seguenti conclusioni:
\begin{itemize}
\item Non vi è evidenza di un legame associativo nel condizionare l'assorbimento del mannitolo alla tipologia di HIV del paziente, né alla presenza/assenza di disturbi intestinali. Tale legame risulta non significativo anche se \textit{stato} e \textit{disturbo} vengono considerate contemporaneamente.
\item Vi è forte evidenza di un moderato legame associativo tra la tipologia di HIV e la presenza di disturbi intestinali.
\item Vi è lieve evidenza di una differenza di variabilità dell'assobimento del lattosio, condizionando alla presenza/assenza di disturbo intestinale del paziente. La bassa significatività, tuttavia, non può portare a conclusioni certe sull'esistenza di tale legame.
\item Altri legami associativi tra i vari gruppi di pazienti e la variabile \textit{lattosio} non risultano significativi.
\end{itemize}
