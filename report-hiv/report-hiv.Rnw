\documentclass{article}

\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath, amssymb}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{float} % float option [H]
\usepackage{booktabs} % prettier tables
\usepackage{pdfpages} % \includepdf for cover page
% table and figure counter as {section}.{number} %
\usepackage{chngcntr} 
\counterwithin{table}{section}
\counterwithin{figure}{section}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% citations and hyperlinks style
\usepackage{csquotes}
\usepackage[style=numeric, backend=bibtex, url=false, isbn =false, doi=false, eprint=false, sorting=none]{biblatex}
\usepackage[hidelinks]{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor=black!50!black,
    citecolor=blue!50!black,
    urlcolor=blue!80!black
}

\addbibresource{library.bib}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\includepdf{./cover/cover.pdf}

<<knitr-config, echo=FALSE, cache = FALSE, warning = F, message = F>>=
library(knitr)
library(magrittr) # operatore di pipe %>%
library(kableExtra) # opzioni aggiuntive kable
library(ggplot2)
library(gridExtra) # side by side plots in ggplot2
library(car) # Test di Levene, ...
library(rcompanion) # Cramer's V
library(lmtest) # Test per modelli lineari
# Funzione che genera una tabella LaTeX tramite kable con [H] parametro
# kable_styling per forzare [H]
latexTable = function(tab, ...){
  kable(tab, format = "latex", ..., booktabs = T) %>% 
    kableExtra::kable_styling(latex_options ="HOLD_position") 
}

# Opzioni globali per i chunk
knitr::opts_chunk$set(echo = FALSE, fig.pos='H', fig.align='center', fig.height = 3.2, fig.width = 3.2, fig.path="./figure/", tidy = FALSE, out.extra = '', cache = T)

# Kable fa output dei valori NA come "-"
options(knitr.kable.NA = "-")

# Rimuove i warning per "incompatible color definition"
knit_hooks$set(document = function(x) {sub('\\usepackage[]{color}', '\\usepackage{xcolor}', x, fixed = TRUE)})

# Funzione che implementa il qqplot in ggplot2
ggplot_qqplot = function(vec, data = NULL){
  # following four lines from base R's qqline()
  y = quantile(vec[!is.na(vec)], c(0.25, 0.75))
  x = qnorm(c(0.25, 0.75))
  slope = diff(y)/diff(x)
  int = y[1L] - slope * x[1L]
  
  # Inclusione di data opzionale, per poter utilizzare facet_grid e facet_wrap
  if(is.null(data)){
    d = data.frame(resids = vec)
  }  
  else{
    d = cbind(data, resids = vec)
  }  
    ggplot(d, aes(sample = resids)) + stat_qq() + geom_abline(slope = slope, intercept = int)
}
@

<<data, results='hide', message = F, cache = F>>=
hiv = read.table("./data/hiv.txt", header = T)
hiv$stato = as.factor(hiv$stato)
hiv$disturbo = as.factor(hiv$disturbo)
attach(hiv)
@

<<intro, child="./analisi/intro.Rnw">>=
@

<<espl, child="./analisi/esplorativa.Rnw">>=
@

<<ancova, child = "./analisi/anova2v.Rnw">>=
@

<<crop, cache = F>>=
# Script per rimuovere gli spazi bianchi dalle immagini prodotte da knitr
if(.Platform$OS.type == "unix"){
  system(paste('for FILE in ./figure/*.pdf; do
    pdfcrop "${FILE}" "${FILE}"
  done'))
}
@

<<end, cache = F>>=
detach(hiv)
@

\newpage

\printbibliography
\end{document}