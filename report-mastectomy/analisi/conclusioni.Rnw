\section{Conclusioni}
Sia con il {\em log-rank test}, sia con il modello a rischi proporzionali di Cox, la metastasi del cancro non risulta significativa per la sopravvivenza dei pazienti. In particolare, l'intervallo di confidenza al $95\%$ stimato per l'{\em Hazard Ratio} è $\left(\Sexpr{round(IcHR[1],2)}, \Sexpr{round(IcHR[2],2)}\right)$.
